//********************************************************************
//  BMIGUI.java     
//
//  Wyznacza BMI w GUI.
//********************************************************************

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;


public class BMIGUI
{
   private int WIDTH = 280;
   private int HEIGHT = 120;

   private JFrame frame;
   private JPanel panel;
   private JLabel wzrostLabel, wagaLabel, BMILabel, wynikLabel;
   private JTextField wzrost, waga;
   private JButton oblicz;
   private JLabel stateLabel;

   //-----------------------------------------------------------------
   //  Ustawia GUI.
   //-----------------------------------------------------------------
   public BMIGUI()
   {
      frame = new JFrame ("Kalkulator BMI");
      frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);

      //tworzenie etykiety dla pol tekstowych wzrostu i wagi 
      wzrostLabel = new JLabel ("Twoj wzrost w metrach:");
      wagaLabel = new JLabel ("Twoja waga w kilogramach: ");

      //stworz etykiete "to jsest twoje BMI" 
      BMILabel = new JLabel ("To jest twoje BMI");
      //stworz etykiete wynik dla wartosci BMI
      wynikLabel = new JLabel ("Wynik");
      // stworz JTextField dla wzrostu
      wzrost = new JTextField();
      wzrost.setPreferredSize(new Dimension(40,15));
      // stworz JTextField dla wagi
      waga = new JTextField();
      waga.setPreferredSize(new Dimension(40,15));
      
      // stworz przycisk, ktory po wcisnieciu policzy BMI
      oblicz = new JButton("Oblicz BMI");
      // stworz BMIListener, ktory bedzie nasluchiwal czy przycis zostal nacisniety 
      oblicz.addActionListener (new BMIListener());
      // ustawienia JPanel znajdujacego sie na JFrame 
      panel = new JPanel();
      panel.setPreferredSize (new Dimension(WIDTH, HEIGHT));
      panel.setBackground (Color.yellow);
      
      // Stworz Jlabel dla stateLabel
      stateLabel = new JLabel("stan");
      
      //dodaj do panelu etykiete i pole tekstowe dla wzrostu
      panel.add(wzrostLabel);
      panel.add(wzrost);
      //dodaj do panelu etykiete i pole tekstowe dla wagi
      panel.add(wagaLabel);
      panel.add(waga);
      //dodaj do panelu przycisk
      panel.add(oblicz);
      //dodaj do panelu etykiete BMI
      panel.add(BMILabel);
      //dodaj do panelu etykiete dla wyniku
      panel.add(wynikLabel);
      //dodaj etykiete dla stanu
      panel.add(stateLabel);
      //dodaj panel do frame 
      frame.getContentPane().add (panel);
   }

   //-----------------------------------------------------------------
   //  Wyswietl frame aplikacji podstawowej
   //-----------------------------------------------------------------
   public void display()
   {
      frame.pack();
      frame.show();
   }

   //*****************************************************************
   //  Reprezentuje action listenera dla przycisku oblicz.
   //*****************************************************************
   private class BMIListener implements ActionListener
   {
      //--------------------------------------------------------------
      //  Wyznacz BMI po wcisnieciu przycisku
      //--------------------------------------------------------------
      public void actionPerformed (ActionEvent event)
      {
         String wzrostText, wagaText;
         double wzrostVal, wagaVal;
         double bmi;

	 //pobierz tekst z pol tekstowych dla wagi i wzrostu
         try //jesli zle wczytaja sie pola z wzrostem i waga
         {
	         wzrostText = wzrost.getText();
	         wagaText = waga.getText();
	         //Wykorzystaj Integer.parseInt do konwersji tekstu na integer
	         wzrostVal = Double.parseDouble(wzrostText);
	         wagaVal = Double.parseDouble(wagaText);
		 //oblicz  bmi = waga / (wzrost)^2
	         bmi = wagaVal /((wzrostVal*wzrostVal));
	         bmi = Math.round(bmi*100)/100.0;
		 //Dodaj wynik do etykiety dla wyniku.
		 // Wykorzystaj Double.toString do konwersji double na string.
	         wynikLabel.setText("Wynik " +Double.toString(bmi));
	         changeStateLabel(bmi);
         }
         catch (Exception e)
         {
        	 stateLabel.setText("Wprowadziles bledne dane");
         }
	
      }
   }
   private void changeStateLabel(double bmi)
   {
	   if (bmi < 19.0)
		   stateLabel.setText("Masz niedowage");
	   if (bmi >= 19.0 && bmi <= 25.0)
		   stateLabel.setText("Twoje BMI jest w normie");
	   if (bmi > 25.0 && bmi < 30.0)
		   stateLabel.setText("Masz nadwage");
	   if (bmi >= 30.0)
		   stateLabel.setText("Jestes otyly");
   }
}

